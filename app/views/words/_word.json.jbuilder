json.extract! word, :id, :game_id, :text, :player_id, :created_at, :updated_at
json.url word_url(word, format: :json)
