class GamesController < ApplicationController
  before_action :set_game, only: %i[show edit update destroy]

  def new_join_game; end

  def next_turn
    game = Game.find(params[:game_id])
    won_turn = params[:won_turn]
    player = Player.find(session[:current_player_id])

    if won_turn == 'true'
      game.current_word.update(used: true)
      game.update(t1_score: game.t1_score + 1, current_word: game.words.where(used: false).sample)
    else
      game.update(current_word: game.words.where(used: false).sample)
    end
    game.pick_next_player

    redirect_to game
  end

  def start_game
    game = Game.find(params[:game_id])
    game.set_players_positions
    game.update(
      status: 'round1',
      turn: 0, current_team: 1,
      current_player_id: game.players.find_by(position: 0, team: 1).id,
      t1_score: 0,
      t2_score: 0,
      current_word: game.words.where(used: false).sample
    )

    redirect_to game
  end

  def switch_team
    puts "SESSION #{session[:current_player_id]}"
    player = Player.find(session[:current_player_id])
    player.update(team: player.team == 1 ? 2 : 1)

    redirect_to player.game
  end

  def join_game
    puts("player #{params[:player_name]} is joining game #{params[:game_id]}")

    player = Player.create!(name: params[:player_name], game_id: params[:game_id], team: 1)
    session[:current_player_id] = player.id

    puts "CREATED PLAYER #{player.id} - #{player.name}"

    game = Game.find(params[:game_id])

    redirect_to game
  end

  # GET /games
  # GET /games.json
  def index
    @games = Game.all
  end

  # GET /games/1
  # GET /games/1.json
  def show
    @player = Player.find(session[:current_player_id])

    @team1 = @game.players.where(team: 1).sort_by(&:position)
    @team2 = @game.players.where(team: 2).sort_by(&:position)
  end

  # GET /games/new
  def new
    @game = Game.new
  end

  # GET /games/1/edit
  def edit; end

  # POST /games
  # POST /games.json
  def create
    player = Player.create!(name: game_params[:owner], team: 1)
    session[:current_player_id] = player.id
    @game = Game.new(owner: player, status: 'lobby')

    respond_to do |format|
      if @game.save
        player.update!(game_id: @game.id)
        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_game
    @game = Game.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def game_params
    params.require(:game).permit(:owner)
  end
end
