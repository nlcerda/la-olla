class Game < ApplicationRecord
  belongs_to :owner, class_name: 'Player', foreign_key: 'owner'
  has_many :players
  has_many :words
  belongs_to :current_player, class_name: 'Player', foreign_key: 'current_player_id', optional: true
  belongs_to :last_player, class_name: 'Player', foreign_key: 'last_player_id', optional: true

  belongs_to :current_word, class_name: 'Word', foreign_key: 'current_word_id', optional: true

  def pick_next_player
    next_player_team = current_player.team == 1 ? 2 : 1

    next_player = nil
    if last_player.nil?
      next_player = players.find_by(team: next_player_team, position: 0)
    else
      next_position = last_player.position + 1
      next_player = players.find_by(team: next_player_team, position: next_position)
      next_player = players.find_by(team: next_player_team, position: 0) if next_player.nil?
    end
    update(last_player: current_player)
    update(current_player: next_player)
  end

  def set_players_positions
    team1_players = players.where(team: 1)
    team2_players = players.where(team: 2)

    team1_players.each_with_index do |p, i|
      p.update(position: i)
    end

    team2_players.each_with_index do |p, i|
      p.update(position: i)
    end
  end
end
