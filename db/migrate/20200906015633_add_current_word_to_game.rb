class AddCurrentWordToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :current_word_id, :int
  end
end
