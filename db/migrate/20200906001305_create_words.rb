class CreateWords < ActiveRecord::Migration[5.2]
  def change
    create_table :words do |t|
      t.string :game_id
      t.string :text
      t.integer :player_id

      t.timestamps
    end
  end
end
