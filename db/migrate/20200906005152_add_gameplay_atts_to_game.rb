class AddGameplayAttsToGame < ActiveRecord::Migration[5.2]
  def change
    add_column :games, :turn, :integer
    add_column :games, :current_team, :integer
    add_column :games, :current_player_id, :integer
    add_column :games, :t1_score, :integer
    add_column :games, :t2_score, :integer
  end
end
