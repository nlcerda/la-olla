class AddUsedToWord < ActiveRecord::Migration[5.2]
  def change
    add_column :words, :used, :boolean, default: false
  end
end
