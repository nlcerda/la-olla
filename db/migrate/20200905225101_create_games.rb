class CreateGames < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'uuid-ossp'
    enable_extension 'pgcrypto'

    create_table :games, id: :uuid do |t|
      t.string :owner

      t.timestamps
    end
  end
end
