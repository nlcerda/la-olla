class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.string :name
      t.string :game_id
      t.string :session
      t.integer :team

      t.timestamps
    end
  end
end
