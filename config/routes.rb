Rails.application.routes.draw do
  resources :words
  resources :players
  resources :games
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'switch_team', to: 'games#switch_team'
  post 'add_word', to: 'players#add_word'

  get 'games/:game_id/join', to: 'games#new_join_game', as: 'join_to_game'
  post 'games/:game_id/join', to: 'games#join_game', as: 'join_game'

  get 'games/:game_id/next_turn(/won_turn)', to: 'games#next_turn', as: 'next_turn'

  get 'games/:game_id/start_game', to: 'games#start_game', as: 'start_game'
  root to: 'games#new'
end
